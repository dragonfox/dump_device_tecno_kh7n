#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-KH7n.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-KH7n-user \
    lineage_TECNO-KH7n-userdebug \
    lineage_TECNO-KH7n-eng
